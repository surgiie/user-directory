<div class="row" id="site-notifications">
    <div class="col-sm-12">
        @if(Session::has('success'))
            <div class="site-alerts">
               <p class="text-center alert alert-success">{{ Session::get('success') }}</p>
            </div>
        @endif



        @if (count($errors) > 0)
            <div class="alert alert-danger text-center">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>