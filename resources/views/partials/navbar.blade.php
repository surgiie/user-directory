<nav class="navbar navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">User Directory</a>
        </div>


        <div class="collapse navbar-collapse" id="navigation">
            <ul class="nav navbar-nav">
                    
                @if (Auth::guest())
                    <li class="{{ Request::segment(1) === 'about' ? 'active' : '' }}">
                        <a href="about">
                            About
                            <i class="fa fa-address-book" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="{{ Request::segment(1) === 'login' ? 'active' : '' }}">
                        <a href="/login">
                            Sign in
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="{{ Request::segment(1) === 'register' ? 'active' : '' }}">
                        <a href="/register">
                            Sign Up
                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif


            </ul>
            @if(Auth::check())
            <div class="col-sm-3 col-md-3 pull-right">
                <form action="search" method="GET" class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" value="{{isset($_GET['search_phrase']) ?$_GET['search_phrase'] :""}}" name="search_phrase">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            @endif
        </div>

    </div>

</nav>