<footer>
    <div class="container">
        <p class="text-center">
            &copy;{{date('Y')}} User Directory.
        </p>
    </div>
</footer>