@extends('layouts.app')

@section('header')

<div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3 header-panel panel panel-default">
        <div class="panel-body text-center">
          <i class="fa fa-address-book fa-3x" aria-hidden="true"></i>
          <h3>About User Directory</h3>
        </div>
      </div>
    </div>
</div>

@endsection


@section('content')
  <div class="container">
    <div class="row">

      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title text-center">What we are and what we offer</h3>
          </div>
          <div class="panel-body">
            <div class="col-lg-6">
              <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe molestias, iure aliquam accusamus reiciendis! Tenetur molestiae quidem, ipsam, id tempora, laboriosam animi quibusdam hic natus incidunt quo magnam nobis aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum earum, numquam, vitae neque modi repudiandae commodi blanditiis tempore explicabo veniam odit est optio dolorem! Accusamus totam temporibus quod ut atque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dolores ea vero consectetur expedita blanditiis sequi delectus maiores mollitia, at repellat consequatur aliquid, eligendi voluptates impedit eius non vel quaerat.
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe molestias, iure aliquam accusamus reiciendis! Tenetur molestiae quidem, ipsam, id tempora, laboriosam animi quibusdam hic natus incidunt quo magnam nobis aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum earum, numquam, vitae neque modi repudiandae commodi blanditiis tempore explicabo veniam odit est optio dolorem! Accusamus totam temporibus quod ut atque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dolores ea vero consectetur expedita blanditiis sequi delectus maiores mollitia, at repellat consequatur aliquid, eligendi voluptates impedit eius non vel quaerat.
              </p>
            </div>
            <div class="col-lg-6">
               <img src="http://1u88jj3r4db2x4txp44yqfj1.wpengine.netdna-cdn.com/wp-content/uploads/2012/02/contacts.jpg" alt="Address Book" class="img-responsive center-block">
            </div>  
          </div>
        </div>
      </div>
    
    </div>
  </div>
@endsection