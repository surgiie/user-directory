@extends('layouts.app')

@section('header')

<div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3 header-panel panel panel-default">
        <div class="panel-body text-center">
          <i class="fa fa-search fa-3x" aria-hidden="true"></i>
          <h3>Finding people has never been easier!</h3>
        </div>
      </div>
    </div>
</div>

@endsection


@section('content')
  @if(isset($users))
    <div class="container">
      <div class="row">
          {{-- show 3 random users for  logged in users else a join header--}}
          @if(Auth::check())
            @foreach($users as $user)
            <div class="col-sm-4">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">{{$user->name}}</h3>
                </div>
                <div class="panel-body text-center">
                  <img src="{{ $user->image_url == "" ? "https://www.victoria147.com/wp-content/uploads/2014/10/user-avatar-placeholder-300x300.png" : $user->image_url}}" alt="Faker Image" class="center-block img-responsive"/>
                  <p>
                    <a href="mailto:{{$user->email}}" class="theme-color" style="word-break: break-all">
                      <h4>{{$user->email}}</h4>
                    </a>
                  </p>
                  <h4>{{$user->country}}</h4>
                </div>
              </div>
            </div>
            @endforeach
          @else
            <div class="text-center">
              <a href="register" class="theme-color">
                <i class="fa fa-user-plus fa-3x" aria-hidden="true"></i>
                <h1>Join the world's best online user directory</h1>
              </a> 
            </div>
          @endif
      </div>
    </div>
  @endif
@endsection