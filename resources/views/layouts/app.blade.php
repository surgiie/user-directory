<!DOCTYPE html>
<html>
	<head>
		<title>User Directory Assesment</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="css/app.css">
	</head>
	
	<body class="@yield('class')">
		<div id="app">

			@include('partials.navbar')
			{{-- Since laravels register/login pages has errors display automatically
			only include custom site notifications on other pages --}}
			@if(!Request::segment(1)=='login' || !Request::segment(1)=='register')
				@include('partials.notifications')
			@endif
			<div class="page-header">
				@yield('header')
			</div>
		

			<div class="page-content">
				@yield('content')
			</div>

			@include('partials.footer')

		</div>

		<script>
	        window.Laravel = {!! json_encode([
	            'csrfToken' => csrf_token(),
	        ]) !!};
    	</script>

		<script src="js/app.js"></script>

		{{-- Only needed for register page --}}
		@if(Request::segment(1)=='register')
			{{-- 
			Determining Timezone can be a bit tricky as far as i see. It seems js is
	`		more reliable than serverside.

			Will use momentjs to determine timezone and pass as a hidden input via jquery

			http://momentjs.com/timezone/docs/#/using-timezones/guessing-user-timezone/

			 --}}
			<script>
			    $(document).ready(function($)
			    {
			        $tz = moment.tz.guess();
			        $('input[name=timezone]').val($tz);
			    });
			</script>
		@endif
	</body>
</html>
