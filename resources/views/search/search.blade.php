@extends('layouts.app')

@section('header')

<div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3 header-panel panel panel-default">
        <div class="panel-body text-center">
          <i class="fa fa-search fa-3x" aria-hidden="true"></i>
          <h3>Search Results</h3>
        </div>
      </div>
    </div>
</div>

@endsection


@section('content')
  @if(count($users) > 0)
    <div class="container">
      <div class="row">
          {{-- only show if the user is authenticated --}}
          @if(Auth::check())
            {{-- if domain is test2.com show a html table --}}
            @if(Request::server('SERVER_NAME')== 'test2.com')
              <div class="table-responsive">
                <table class="table table-hover">
                  <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>User</th>
                  </tr>
                  @foreach($users as $user)
                    <tr>
                      <td>
                        <img src="{{ $user->image_url == "" ? "https://www.victoria147.com/wp-content/uploads/2014/10/user-avatar-placeholder-300x300.png" : $user->image_url}}" alt="Faker Image" class="center-block img-responsive"/>
                      </td>
                      <td>{{$user->name}}</td>
                      <td>
                        <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                      </td>
                      <td>{{$user->country}}</td>
                    </tr> 
                  @endforeach
                </table>
              </div>
              
              {{$users->appends(['search_phrase'=>Request::input('search_phrase')])->links()}}


            {{--Else if its not use bootstrap panels for every other domain --}}
            @else
              @foreach($users as $user)
              <div class="col-md-4" style="margin-top: 1em;">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h2 class="panel-title text-center">{{$user->name}}</h2>
                  </div>
                  <div class="panel-body text-center">
                    <img src="{{ $user->image_url == "" ? "https://www.victoria147.com/wp-content/uploads/2014/10/user-avatar-placeholder-300x300.png" : $user->image_url}}" 
                      alt="Faker Image" class="center-block img-responsive"/>
                    <p>
                      <a href="mailto:{{$user->email}}" class="theme-color" style="word-break: break-all">
                        <h4>{{$user->email}}</h4>
                      </a>
                    </p>
                    <h5>{{$user->country}}</h5>
                  </div>
                </div>
              </div>
              @endforeach
              {{$users->appends(['search_phrase'=>Request::input('search_phrase')])->links()}}
            @endif
          
          @endif
      </div>
    </div>
    @else
      <div class="col-sm-12">
          <div class="alert alert-warning text-center" role="alert">
            <i class="fa fa-info-circle fa-3x" aria-hidden="true"></i>
            <h1>No results were found</h1>
          </div>   
      </div>
  @endif
@endsection