<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//check for a search first
Route::get('search', 'PagesController@search');

//any other pages will be returned if they exist in /views/pages
Route::get('/{page?}', 'PagesController@show')->where('page','.*');
