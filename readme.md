#IConnect, Inc interview assesment.
Laravel app as part of the iConnect, Inc. Interview process:

The goal of this test is to assert (to some degree) your coding skills. You're given a simple problem, so you can focus on showcasing development techniques. 

Requirements 

We'd like you to build a simple user directory with Laravel. UI and code must be written in english. UI doesn't have to be pretty, but it has to work. 

Features 

- Signup 
- Login 
- Logout 
- User search 
- Different themes for domain names 

Rules: 

- Anyone can signup 
- User search must be done with a single text input field 
- User search can match users both by name or email 
- Non logged-in users can fill in and submit the registration form 
- Only logged-in users can run the search and view its results 
- Logged-in users must not view the signup or login forms 

- Accessing the website using any domain name, but test2.com, should show the main theme 

- Accessing the website using test2.com domain name should show theme2, inherited from the main theme (user search results have a different table markup (html) for example, all other pages are the same) 


User data: 

- name (required, min 3 characters) 
- email (required, unique, must be a valid email address) 
- password (required, any character is allowed, min 6 characters with at least 1 digit) 
- country (required) 
- timezone (required, don't ask the user, we must determine it ourselves) 


#Starting up application:

1. clone repo
2. cd into project
3. run "composer install" to install application dependencies
4. set up .env file/database
5. run "php artisan migrate" to migrate tables
6. run "php artisan db:seed" to insert test/faker data
7. run "php artisan serve" to start up application
or for test2.com scenario "php artisan serve --host=test2.com"
