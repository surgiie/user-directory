<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as RequestFascade;
class PagesController extends Controller
{
   
    /*  
    * Return a view for an pages that exist in /views/pages/
    */
   
    public function show($page)
    {
        
        $page = str_replace('/', '.', $page);

        if (! str_contains($page, 'pages'))
        {
            $page = ($page === '.') ? 'pages.index' : sprintf('pages.%s', $page);
        }

        // make sure it exists in pages
        if(!view()->exists($page)){
        
            abort(404);
        }
      
        switch ($page) {
            case 'pages.index':
                //return 3 random users for part of UI for home page
                $users = \App\User::inRandomOrder()->take(3)->get();
                return view($page)->with(['users'=>$users]);
                break;
            default:
                return view($page);
                break;
        }
 
        
 
    }

    //search users
    public function search(Request $request)
    {   
        
        // $this->validate($request,['search_phrase'=>'required']); just return all results if no search field is given

        $search_phrase = $request->search_phrase;
      
        $users = \App\User::searchByNameOrEmail($search_phrase)->paginate(15);
        
    

        return view('search.search',['users'=>$users]);
    }


    
}
